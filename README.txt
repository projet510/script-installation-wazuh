Script d'installation automatisée de Wazuh sur Debian 11

Ce script permet d'installer automatiquement la dernière version de Wazuh sur un système Debian 11.

Prérequis:

    Avoir les privilèges administrateur sur le système (sudo ou root).
    Avoir un accès internet.

Utilisation:

    Téléchargez le script sur le système cible :

curl "liens vers notre script >> install_Wazuh.sh"

    Donner les droits d'exécution au script:

chmod +x install_Wazuh.sh

    Exécuter le script en tant qu'utilisateur administrateur :

sudo ./install_Wazuh.sh

Le script effectuera les étapes suivantes :

    Téléchargement de la clé GPG de Wazuh.
    Ajout de la clé GPG à la liste des clés APT.
    Ajout du dépôt Wazuh à la liste des sources APT.
    Mise à jour de la liste des paquets.
    Installation de wazuh-manager.
    Configuration d'elasticsearch en renommant le nodename.
    Démarrage des services Wazuh-manager.

 Il est conseillé de tester ce script en premier lieu sur un environnement de test avant de l'utiliser en production ;)

Liens utiles: 

    Site web de Wazuh : https://wazuh.com/
    Documentation de Wazuh : https://documentation.wazuh.com/
    Dépôt Gitlab du script : ...

#TeamProjet5 
